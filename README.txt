Module installation
===================
Follow the standard drupal installation procedure and copy the module files to 
sites/xxx/modules/event_webform/

You should then be able to enable the Event Webform module in the
admin interface.

